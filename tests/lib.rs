#[cfg(test)]
mod bean_tests;
mod roast_tests;

use rand::RngCore;
pub fn random_string() -> String {
    let mut rbuff = [0u8; 32];
    let mut rng = rand::thread_rng();
    rng.fill_bytes(&mut rbuff);
    let bean_name = hex::encode(rbuff);
    println!("random string: {}", bean_name.to_string());
    bean_name.to_string()
}
