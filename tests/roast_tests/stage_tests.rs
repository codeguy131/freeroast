use anyhow::Result;
use freeroast::{
    beans_in, end_roast, first_crack_end, first_crack_start, get_bean_name, get_starting_weight,
    init_db, Bean, Roast, TEST_DB_FILE,
};

use std::{
    fs::{remove_file, OpenOptions},
    io::{BufWriter, Seek, SeekFrom, Write},
    path::Path,
};

use crate::random_string;

#[test]
fn test_stages() -> Result<()> {
    std::env::set_var("TESTING", "true");
    let tmp_pth = Path::new("/tmp/test_file");
    let tmp_fd = OpenOptions::new()
        .create(true)
        .truncate(true)
        .write(true)
        .read(true)
        .open(tmp_pth)?;

    let buff2 = Vec::new();
    let _stdin = BufWriter::new(buff2);
    let mut conn = init_db(TEST_DB_FILE)?;

    let nn = random_string();
    let bean = Bean {
        bean_nickname: nn.clone(),
        amount_in_stock: 12123.3223,
        id: uuid::Uuid::new_v4().to_string(),
        ..Default::default()
    };
    bean.insert(&mut conn)?;

    let mut new_bean = Bean::default();
    let mut reader = BufWriter::new(tmp_fd);
    reader.write_all(nn.as_bytes())?;
    new_bean.bean_nickname = get_bean_name(&mut reader.buffer()).unwrap();
    let new_bean = new_bean.get(&mut conn)?;

    assert!(new_bean.bean_nickname == nn);

    let mut roast = Roast::new(new_bean);
    reader.seek(SeekFrom::End(0))?;
    reader.write_all("33.4543".as_bytes())?;
    roast.starting_weight = get_starting_weight(&mut reader.buffer())?;
    reader.seek(SeekFrom::End(0))?;
    reader.write_all(b"\n")?;
    beans_in(&mut reader.buffer(), &mut roast)?;
    reader.seek(SeekFrom::End(0))?;
    first_crack_start(&mut reader.buffer(), &mut roast)?;
    first_crack_end(&mut reader.buffer(), &mut roast)?;
    end_roast(&mut reader.buffer(), &mut roast)?;

    remove_file(tmp_pth)?;

    Ok(())
}
