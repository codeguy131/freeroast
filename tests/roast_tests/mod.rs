mod stage_tests;

use super::random_string;
use anyhow::Result;
use freeroast::{init_db, time_now, Bean, Roast, RoastEvent, TEST_DB_FILE};

#[test]
fn test_roast() -> Result<()> {
    std::env::set_var("TESTING", "true");
    let db_file = TEST_DB_FILE;
    let mut conn = init_db(db_file)?;
    let mut bean: Bean = Bean::default();
    bean.id = uuid::Uuid::new_v4().to_string();
    bean.bean_nickname = random_string();
    bean.country_of_origin = "Rwanda".to_string();
    bean.region = "test_region".to_string();
    bean.grower = "test_grower".to_string();
    bean.amount_in_stock = 456.32;

    bean.insert(&mut conn)?;
    let mut roast = Roast::new(bean.clone().get(&mut conn)?);

    roast.starting_weight = 11.92;

    roast.insert(&mut conn).expect("Problem inserting roast");

    let mut new_roast = Roast::new(bean.get(&mut conn)?);
    new_roast.starting_weight = 32.4432;
    new_roast.second_crack_start = Some(time_now());
    new_roast.second_crack_finish = Some(time_now());
    RoastEvent::BeansIn().handle(&mut new_roast)?;
    RoastEvent::FirstCrackStart().handle(&mut new_roast)?;
    RoastEvent::FirstCrackEnd().handle(&mut new_roast)?;
    RoastEvent::SecondCrackStart().handle(&mut new_roast)?;
    RoastEvent::SecondCrackEnd().handle(&mut new_roast)?;
    RoastEvent::EndRoast().handle(&mut new_roast)?;

    assert_eq!(444.4 as f32, roast.bean.amount_in_stock);
    roast.delete(&mut conn)?;
    Ok(())
}

#[test]
fn test_new_roast() -> Result<()> {
    std::env::set_var("TESTING", "true");
    let mut bean = Bean::default();
    let mut conn = init_db(TEST_DB_FILE).expect("DB init problem");
    bean.id = uuid::Uuid::new_v4().to_string();
    bean.bean_nickname = random_string();
    println!("{}, {}", bean.id, bean.bean_nickname);
    bean.amount_in_stock = 8000.3232;
    bean.insert(&mut conn).unwrap();
    let mut roast = Roast::new(bean);
    roast.starting_weight = 32.32;
    let event = RoastEvent::BeansIn();
    event.handle(&mut roast)?;
    RoastEvent::FirstCrackStart().handle(&mut roast)?;
    RoastEvent::FirstCrackEnd().handle(&mut roast)?;
    RoastEvent::EndRoast().handle(&mut roast)?;
    roast.delete(&mut conn)?;
    Ok(())
}

#[test]
fn test_timenow() -> std::io::Result<()> {
    let ts = time_now();
    assert_ne!(ts, time_now());
    Ok(())
}
