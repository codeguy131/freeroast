use super::random_string;
use freeroast::{init_db, Bean, TEST_DB_FILE};

#[test]
fn test_bean() {
    let mut conn = init_db(TEST_DB_FILE).expect("Problem initializing database");

    let mut bean = Bean::default();
    bean.id = uuid::Uuid::new_v4().to_string();
    bean.bean_nickname = random_string();
    bean.country_of_origin = "Rwanda".to_string();
    bean.region = "test_region".to_string();
    bean.grower = "test_grower".to_string();
    bean.amount_in_stock = 456.32;

    bean.insert(&mut conn).expect("Problem inserting bean data");

    let new_bean = bean.get(&mut conn).unwrap();
    assert_eq!(new_bean.id, bean.id);
    bean.delete(&mut conn).unwrap()
}
