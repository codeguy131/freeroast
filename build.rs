use std::{fs::create_dir_all, path::Path, process::Command};

fn main() {
    let c_src = Path::new("./src/ext");
    let ev = std::env::var("OUT_DIR").unwrap();
    let out_dir = Path::new(ev.as_str()).join("libs");
    if !out_dir.exists() {
        create_dir_all(out_dir.to_owned()).unwrap();
    }

    // Compile C input program
    Command::new("cc")
        .args(&[
            "-c",
            "-std=c99",
            "-O2",
            "-fPIC",
            "-lncurses",
            "-static",
            "-o",
            &format!("{}/getinput.o", out_dir.to_str().unwrap()),
            &format!("{}/get_input.c", c_src.to_str().unwrap()),
        ])
        .status()
        .expect("Problem compiling c program");

    // Link C input program
    Command::new("ar")
        .args(&["crus", "libgetinput.a", "getinput.o"])
        .current_dir(out_dir.to_owned())
        .status()
        .expect("Problem linking c program");

    println!(
        "cargo:rustc-link-search=native={}",
        out_dir.to_owned().to_str().unwrap()
    );
    println!("cargo:rustc-link-lib=static=getinput");
    println!("cargo:rustc-link-lib=ncurses");
    println!(
        "cargo:rerun-if-changed={}/get_input.c",
        c_src.to_str().unwrap()
    );
}
