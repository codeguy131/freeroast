# Changelog

## [0.1.9] - 2022-12-20

### Added

- Made CLI interface more intuitve during optional 
second crack stage

## [0.1.8] - 2022-12-18

### Fixed

- Fixed Some CLI bugs

### Added

- Added the ability to add beans from the CLI

## [0.1.7] - 2022-12-15

## [Unreleased]

- GUI in the works

### Added

- A main menu to CLI program for checking beans in stock and past roasts

### Fixed

- Improved error handleing and some bug fixes

## [0.1.6] - 2022-02-21

### Fixed

- Timestamps were not being saved into the database properly

## [0.1.5] - 2022-01-26

### Fixed

- New roasts were not keeping track of beans in stock.

## [0.1.4] - 2022-01-24

### Fixed

- New roast could not handle floats for starting weight.
  Changed to f32 for starting weight.

## [0.1.3] - 2022-01-24

### Fixed

- New roast not recording weight. Had to parse the input buffer as u32

## [0.1.2] - 2022-01-24

### Fixed

- New roast was not being inserted at the end of the roast program.
  Works now.

## [0.1.1] - 2022-01-24

### Changed

- Main roast program works and checks for beans
  and will insert the roast into database if the bean
  is already present

## [0.1.0] - 2022-01-19

### Changed

- Started using changelog
