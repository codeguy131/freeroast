FROM rust:slim-buster as build

RUN apt-get -y update && apt-get -y install ncurses-dev

WORKDIR /app

COPY . .

RUN cargo build --release

FROM rust:slim-buster

RUN apt-get -y update && apt-get -y install ncurses-dev

WORKDIR /app

COPY --from=build /app/target/release/roast /app/roast

ENV TERM=xterm-256color

CMD ["/app/roast"]