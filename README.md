# Freeroast

[![pipeline status](https://gitlab.com/codeguy131/freeroast/badges/main/pipeline.svg)](https://gitlab.com/codeguy131/freeroast/-/commits/main) 
![Crates.io](https://img.shields.io/crates/v/freeroast)
![Crates.io](https://img.shields.io/crates/l/freeroast)
![GitLab last commit](https://img.shields.io/gitlab/last-commit/codeguy131/freeroast)
![docs.rs](https://img.shields.io/docsrs/freeroast)
---

### The purpose at the moment is to record coffee roasts and keep track of beans in stock


### [Release Notes](./CHANGELOG.md)

---

## Build with CLI
`git clone https://gitlab.com/codeguy131/freeroast.git`

`cd freeroast`

`cargo build --release`

---

## Install via crates.io
`cargo install freeroast`

---

## Install via git 
(Most recent)

`cargo install --git https://gitlab.com/codeguy131/freeroast`

---

## Build docs
`cargo doc`

---

## Run in docker
In project root

`docker build -t roast:latest .`

`docker run -it roast:latest`

---

## Usage

### Run with cargo:
`cargo run --release`

### Run installed binary
`roast`