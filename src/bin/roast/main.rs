//!
//! Runs the roast CLI
//!
mod cli;

use cli::start_cli;
use freeroast::init_db;
use freeroast::{DB_FILE, TEST_DB_FILE};

use anyhow::Result;

fn main() -> Result<()> {
    // Get args
    let args = std::env::args();

    // Check if in test mode and set DB file to use
    let db_file = match args.into_iter().any(|s| *s == String::from("--testing")) {
        true => {
            println!("Running in testing mode");
            std::env::set_var("TESTING", "true");
            TEST_DB_FILE
        }
        false => DB_FILE,
    };

    // Initialize database
    let mut conn = init_db(db_file).expect("Problem opening database");
    start_cli(&mut conn)?;
    Ok(())
}
