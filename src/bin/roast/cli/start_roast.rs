use anyhow::Result;
use freeroast::{
    beans_in, end_roast, first_crack_end, first_crack_start, get_bean_name, get_starting_weight,
    second_crack_end, second_crack_start, Bean, Roast,
};
use rusqlite::Connection;
use std::io::stdin;

///
/// Starts a new roast cycle
///
pub fn start_roast(conn: &mut Connection) -> Result<()> {
    let stdin = stdin();
    let mut bean = Bean::default();
    bean.bean_nickname = get_bean_name(&mut stdin.lock())?.trim().to_string();
    let bean = bean.get(conn)?;
    if bean.id == "" {
        println!("Bean may not exist");
        return Ok(());
    }

    println!("Bean info: {:?}", bean);

    let start_weight = match get_starting_weight(&mut stdin.lock()) {
        Ok(sw) => sw,
        Err(e) => {
            println!("Problem getting weight: {e}");
            println!("Ending roast now...");
            return Ok(());
        }
    };
    if start_weight > bean.amount_in_stock {
        println!("Not enough of this bean.");
        println!("Ending roast now...");
        return Ok(());
    }
    let mut rst = Roast::new(bean);
    rst.starting_weight = start_weight;

    beans_in(&mut stdin.lock(), &mut rst)?;
    first_crack_start(&mut stdin.lock(), &mut rst)?;
    first_crack_end(&mut stdin.lock(), &mut rst)?;
    if let Ok(None) = second_crack_start(&mut rst) {
        return Ok(());
    }
    if let Ok(None) = second_crack_end(&mut rst) {
        return Ok(());
    }
    end_roast(&mut stdin.lock(), &mut rst)?;

    Ok(())
}
