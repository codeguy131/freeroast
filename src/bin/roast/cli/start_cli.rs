use crate::cli::cli_beans::new_bean;

use super::clear_stdout;
use super::menu;
use super::start_roast;
use anyhow::Result;
use freeroast::{get_all_beans, get_all_roasts};
use rusqlite::Connection;
use std::io::{stdin, stdout, Write};

///
/// Starts the CLI interface
///
pub fn start_cli(conn: &mut Connection) -> Result<()> {
    // Main loop
    let mut buff = String::new();
    clear_stdout();

    // Display menu
    super::menu();

    // Main loop
    loop {
        buff.clear();
        print!("Enter command: ");
        stdout().flush().expect("Problem flushing stdout");

        // Get command from user
        if let Err(e) = stdin().read_line(&mut buff) {
            return Err(anyhow::Error::from(e));
        }

        // Handle command from user
        match buff.trim() {
            "add_bean" => {
                clear_stdout();
                if let Err(e) = new_bean(&mut stdin().lock(), conn) {
                    println!("{e}");
                }
            }
            "start_roast" => {
                clear_stdout();
                start_roast(conn)?;
            }
            "exit" => {
                println!("Exiting now...");
                break;
            }
            "show_beans" => {
                let beans = get_all_beans(conn)?;
                println!("{:?}", beans);
            }
            "show_roasts" => {
                let roasts = get_all_roasts(conn)?;
                println!("{:?}", roasts);
            }
            "help" => menu(),

            _ => println!("Invalid command"),
        }
    }
    Ok(())
}
