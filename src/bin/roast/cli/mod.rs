//!
//! CLI commands
//!
mod cli_beans;
mod menu;
mod start_cli;
mod start_roast;

use std::process::Command;

pub use menu::menu;
pub use start_cli::start_cli;
pub use start_roast::start_roast;

///
/// Clears standard output
///
#[inline]
fn clear_stdout() {
    if let Err(err) = Command::new("clear").status() {
        println!("WARNING...clear command failed: {err}");
    }
}
