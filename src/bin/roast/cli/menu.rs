pub fn menu() {
    let ms = String::from(
        r#"
FREEROAST CLI

start_roast:    Starts roast
add_bean:       Add a new bean to database
exit:           Kills program
show_beans:     Shows all beans in database
show_roasts:    Shows all roasts in database
help:           Prints this menu
    "#,
    );
    println!("{}", ms);
}
