use std::io::{stdout, BufRead, Read, Write};

use anyhow::Result;
use freeroast::Bean;
use rusqlite::Connection;

///
/// Add new bean from CLI
///
pub fn new_bean<T: Read + BufRead>(stdin: &mut T, conn: &mut Connection) -> Result<Bean> {
    let mut bean = Bean::default();
    print!("Enter bean name: ");
    stdout().flush()?;
    stdin.read_line(&mut bean.bean_nickname)?;
    let mut bean = bean.get(conn)?;
    if bean.id.ne("") {
        return Err(anyhow::Error::msg("Bean already exists"));
    }

    print!("Enter grower: ");
    stdout().flush()?;
    stdin.read_line(&mut bean.grower)?;

    print!("Enter bean country of origin: ");
    stdout().flush()?;
    stdin.read_line(&mut bean.country_of_origin)?;

    print!("Enter region where grown: ");
    stdout().flush()?;
    stdin.read_line(&mut bean.region)?;

    print!("Enter the amount of beans you have on hand: ");
    stdout().flush()?;
    let mut buff = String::new();
    stdin.read_line(&mut buff)?;
    bean.amount_in_stock = match buff.trim().parse::<f32>() {
        Ok(ais) => ais,
        Err(e) => return Err(anyhow::Error::from(e)),
    };

    // Give bean uuid string for id field
    bean.id = uuid::Uuid::new_v4().to_string();

    // Trim newlines from end of bean fields
    let n_bean = Bean {
        id: bean.id,
        bean_nickname: bean.bean_nickname.trim().to_string(),
        amount_in_stock: bean.amount_in_stock,
        grower: bean.grower.trim().to_string(),
        region: bean.region.trim().to_string(),
        country_of_origin: bean.country_of_origin.trim().to_string(),
    };
    if let Err(e) = n_bean.insert(conn) {
        if e.to_string().contains("UNIQUE") {
            return Err(anyhow::Error::msg("ERROR: Bean already exists"));
        }
    }

    println!("Bean added to database");

    Ok(n_bean)
}
