//!
//! A simple coffee roasting app
//!
//! Install the binary using cargo:
//!
//! `cargo install freeroast`
//!
mod beans;
mod db;
mod roast;

pub use beans::Bean;
pub use db::{get_all_beans, get_all_roasts, init_db};
pub use roast::*;

/// Main production sqlite database
pub const DB_FILE: &str = "roastdb.sqlite";

/// Database used for testing
pub const TEST_DB_FILE: &str = "test_roastdb.sqlite";
