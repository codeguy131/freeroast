//!
//! Bean data structure
//!

/// Data structure for beans in stock
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Bean {
    pub id: String,
    pub bean_nickname: String,
    pub country_of_origin: String,
    pub region: String,
    pub grower: String,
    pub amount_in_stock: f32,
}

impl Default for Bean {
    fn default() -> Self {
        Self {
            id: "".to_string(),
            amount_in_stock: 0.0,
            bean_nickname: "".to_string(),
            country_of_origin: "".to_string(),
            region: "".to_string(),
            grower: "".to_string(),
        }
    }
}
