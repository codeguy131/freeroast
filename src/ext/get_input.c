#include <ncurses.h>
#include <stdio.h>

char get_input(char **args) {

  // data
  char ch;
  WINDOW *window = NULL;

  // Init screen
  window = initscr();
  if (window == NULL)
    return 1;
  if ((cbreak() || raw() || start_color() ||
       init_pair(1, COLOR_YELLOW, COLOR_BLACK) ||
       wbkgd(window, COLOR_PAIR(1))) < 0)
    return 1;

  // Add message to screen and get char
  mvaddstr(10, 30, &args[0]);
  if (wrefresh(window) || (ch = wgetch(window)) < 0)
    return 1;

  // Clear and free window
  if ((wclear(window) || endwin()) < 0)
    return 1;

  // Return char from stdin
  return ch;
}