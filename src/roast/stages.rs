use crate::{Bean, Roast, RoastEvent};
use anyhow::Result;
use chrono::NaiveDateTime;
use std::ffi::c_char;
use std::io::BufRead;
use std::io::Read;

// external C function to get a character
// from raw stdin using ncurses
extern "C" {
    fn get_input(args: *const c_char) -> c_char;
}

#[inline]
pub fn get_bean_name<T: Read + BufRead>(stdin: &mut T) -> Result<String> {
    println!("Enter bean name");
    let mut buff = String::new();
    stdin.read_line(&mut buff)?;
    let mut bean = Bean::default();
    bean.bean_nickname = buff.clone();
    return Ok(buff.to_owned());
}

#[inline]
pub fn get_starting_weight<T: Read + BufRead>(stdin: &mut T) -> Result<f32> {
    println!("Enter starting weight");
    let mut buff = String::new();
    stdin.read_line(&mut buff)?;

    match buff.trim().parse::<f32>() {
        Ok(sw) => Ok(sw),
        Err(e) => Err(anyhow::Error::from(e)),
    }
}

#[inline]
pub fn beans_in<T: Read + BufRead>(stdin: &mut T, roast: &mut Roast) -> Result<()> {
    println!("Press enter when beans go in");
    stdin.read_line(&mut String::new())?;
    RoastEvent::BeansIn().handle(roast)?;
    Ok(())
}

#[inline]
pub fn first_crack_start<T: BufRead + Read>(stdin: &mut T, roast: &mut Roast) -> Result<()> {
    println!("Press enter when first crack starts");
    let mut buff = String::new();
    stdin.read_line(&mut buff)?;
    RoastEvent::FirstCrackStart().handle(roast)?;
    Ok(())
}

#[inline]
pub fn first_crack_end<T: Read + BufRead>(stdin: &mut T, roast: &mut Roast) -> Result<()> {
    println!("Press enter when first crack ends");
    stdin.read_line(&mut String::new())?;
    RoastEvent::FirstCrackEnd().handle(roast)?;
    Ok(())
}

#[inline]
pub fn second_crack_start(roast: &mut Roast) -> Result<Option<()>> {
    let msg = format!(
        r#"
        ========================SECOND CRACK OPTIONAL=========================
        Press ENTER when second crack starts or anything else to end roast
        ======================================================================
        "#
    );

    let sc_sel: String = unsafe {
        let c = get_input(msg.as_ptr() as *const i8);
        String::from_utf8(vec![c as u8]).unwrap()
    };

    if sc_sel != "\n" {
        roast.second_crack_start = None;
        RoastEvent::EndRoast().handle(roast)?;
        return Ok(None);
    }

    roast.second_crack_start = Some(NaiveDateTime::from(roast.first_crack_start));
    Some(RoastEvent::SecondCrackStart().handle(roast)?);
    Ok(Some(()))
}

#[inline]
pub fn second_crack_end(roast: &mut Roast) -> Result<Option<()>> {
    let msg = format!(
        r#"
     =========================SECOND CRACK============================
     Press ENTER when second crack ends or anything else to end roast
     =================================================================
        "#
    );

    let inp = unsafe {
        let c = get_input(msg.as_ptr() as *const i8);
        if c == 1 {
            println!("GETTING INPUT FAILED!!!!!");
            println!("Assuming second crack has ended");
            return Ok(Some(RoastEvent::SecondCrackEnd().handle(roast).unwrap()));
        }
        String::from_utf8(vec![c as u8]).unwrap()
    };

    if inp == "\n" {
        Some(RoastEvent::SecondCrackEnd().handle(roast)?);
        return Ok(Some(()));
    }

    RoastEvent::EndRoast().handle(roast)?;
    Ok(None)
}

#[inline]
pub fn end_roast<T: Read + BufRead>(stdin: &mut T, roast: &mut Roast) -> Result<()> {
    println!("Press enter to end roast");
    stdin.read_line(&mut String::new())?;
    RoastEvent::EndRoast().handle(roast)?;
    Ok(())
}
