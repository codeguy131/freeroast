//!
//! Roast structure, methods, and events
//!
mod new_roast;
mod stages;

use crate::Bean;
use chrono::NaiveDateTime;
pub use new_roast::time_now;
pub use stages::{
    beans_in, end_roast, first_crack_end, first_crack_start, get_bean_name, get_starting_weight,
    second_crack_end, second_crack_start,
};
///
/// Roast data structure
///
#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Roast {
    pub id: String,
    pub bean: Bean,
    pub starting_weight: f32,
    pub beans_in: NaiveDateTime,
    pub first_crack_start: NaiveDateTime,
    pub first_crack_finish: NaiveDateTime,
    pub second_crack_start: Option<NaiveDateTime>,
    pub second_crack_finish: Option<NaiveDateTime>,
    pub beans_out: NaiveDateTime,
}

///
/// For handeling events durring the roast
///
pub enum RoastEvent {
    BeansIn(),
    FirstCrackStart(),
    FirstCrackEnd(),
    SecondCrackStart(),
    SecondCrackEnd(),
    EndRoast(),
}
