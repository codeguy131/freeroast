use super::RoastEvent;
pub use crate::db::init_db;
use crate::{Roast, DB_FILE, TEST_DB_FILE};
use chrono::NaiveDateTime;
use rusqlite::Connection;
use std::io::Result;

pub fn time_now() -> NaiveDateTime {
    return chrono::Utc::now().naive_utc();
}

///
/// Handles events during roast
///
impl RoastEvent {
    pub fn handle(self, roast: &mut Roast) -> Result<()> {
        match self {
            RoastEvent::BeansIn() => {
                roast.beans_in = time_now();
                assert!(roast.starting_weight > 0.00);
                println!("Beans put in at {}", roast.beans_in);
            }
            RoastEvent::FirstCrackStart() => {
                roast.first_crack_start = time_now();
                println!("First crack started at {}", roast.first_crack_start);
            }
            RoastEvent::FirstCrackEnd() => {
                roast.first_crack_finish = time_now();
                println!("Fisrt crack ended at {}", roast.first_crack_finish);
            }
            RoastEvent::SecondCrackStart() => match roast.second_crack_start {
                Some(mut start) => {
                    start = time_now();
                    roast.second_crack_start = Some(start);
                    println!(
                        "Second crack started at {}",
                        roast.second_crack_start.unwrap(),
                    );
                }
                None => (),
            },
            RoastEvent::SecondCrackEnd() => match roast.second_crack_start {
                Some(mut finish) => {
                    finish = time_now();
                    roast.second_crack_finish = Some(finish);
                    println!(
                        "Second crack ended at {}",
                        roast.second_crack_finish.unwrap()
                    );
                }
                None => (),
            },
            RoastEvent::EndRoast() => {
                roast.beans_out = time_now();
                let db_file = match std::env::var("TESTING") {
                    Ok(_) => TEST_DB_FILE,
                    Err(_) => DB_FILE,
                };
                println!("Roast ended at {}", roast.beans_out);
                let mut conn = Connection::open(db_file).expect("Problem getting db connection");
                roast.insert(&mut conn).expect("Problem inseting roast");
                drop(conn)
            }
        }
        Ok(())
    }
}
