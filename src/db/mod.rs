//!
//! Database mappings and methods
//! for roasts and beans
//!
mod beans;
mod init_db;
mod roasts;
mod schema;

pub use beans::{get_all_beans, Bean};
pub use init_db::init_db;
pub use roasts::get_all_roasts;
