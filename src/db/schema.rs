//!
//! Returns the database schema as a string
//!
pub fn get_schema() -> String {
    return String::from(
        r#"

            PRAGMA foreign_keys = ON;

            CREATE TABLE IF NOT EXISTS beans (
                id TEXT UNIQUE NOT NULL PRIMARY KEY,
                bean_nickname TEXT NOT NULL UNIQUE,
                country_of_origin TEXT NOT NULL,
                region TEXT NOT NULL,
                grower TEXT NOT NULL,
                amount_in_stock INTEGER NOT NULL
                CHECK(id <> '' AND bean_nickname <> '')
            );

            CREATE TABLE IF NOT EXISTS roasts (
                id TEXT NOT NULL PRIMARY KEY,
                bean_name TEXT NOT NULL,
                starting_weight INTEGER NOT NULL,
                beans_in INTEGER NOT NULL,
                first_crack_start INTEGER NOT NULL,
                first_crack_finish INTEGER NOT NULL,
                second_crack_start INTEGER DEFAULT NULL,
                second_crack_finish INTEGER DEFAULT NULL,
                beans_out INTEGER NOT NULL,
                FOREIGN KEY (bean_name) 
                    REFERENCES beans (bean_nickname)
                    ON DELETE RESTRICT
                CHECK(id <> '' AND bean_name <> '')
            );
        "#,
    );
}
