//!
//! Data structures and some methods for beans
//!

pub use crate::beans::Bean;
use rusqlite::{params, Connection, Result, ToSql};

/// Bean mappings for database
impl Bean {
    /// Insert new bean into database
    pub fn insert(&self, conn: &mut Connection) -> Result<()> {
        let tx = conn.transaction()?;
        tx.execute(
            r#"
                INSERT INTO beans (id, 
                    bean_nickname, 
                    country_of_origin, 
                    region, grower, 
                    amount_in_stock)
                    VALUES (?1, ?2, ?3, ?4, ?5, ?6);
            "#,
            params![
                self.id.to_sql()?,
                self.bean_nickname.to_sql()?,
                self.country_of_origin.to_sql()?,
                self.region.to_sql()?,
                self.grower.to_sql()?,
                self.amount_in_stock,
            ],
        )?;

        // Commit transaction to database
        tx.commit()?;

        Ok(())
    }

    /// Delete bean from database
    pub fn delete(self, conn: &mut Connection) -> Result<()> {
        conn.execute(
            "DELETE FROM beans WHERE ?1 LIKE id;",
            params![self.id.as_str(),],
        )?;

        Ok(())
    }

    /// Get bean info from database
    pub fn get(&mut self, conn: &mut Connection) -> Result<Self> {
        let tx = conn.transaction()?;
        let mut qry = tx
            .prepare(
                r#"
                    SELECT id, bean_nickname, country_of_origin, region, grower, amount_in_stock
                    FROM beans 
                    WHERE bean_nickname LIKE ?1;
                "#,
            )
            .expect("Problem preparing statement");
        let bean_rows = qry.query_map(params![self.to_owned().bean_nickname], |row| {
            Ok({
                self.id = row.get(0)?;
                self.bean_nickname = row.get(1)?;
                self.country_of_origin = row.get(2)?;
                self.region = row.get(3)?;
                self.grower = row.get(4)?;
                self.amount_in_stock = row.get(5)?;
            })
        })?;

        for row in bean_rows {
            row.unwrap()
        }

        Ok(self.to_owned())
    }
}

///
/// Returns a vector conaining all beans in database
///
pub fn get_all_beans(conn: &mut Connection) -> Result<Vec<Bean>> {
    let mut beans: Vec<Bean> = Vec::new();
    let tx = conn.transaction()?;
    let mut qry = tx.prepare(
        r#"
        SELECT * FROM beans;
    "#,
    )?;
    let bean_rows = qry.query_map([], |row| {
        let mut bean = Bean::default();
        bean.id = row.get(0)?;
        bean.bean_nickname = row.get(1)?;
        bean.country_of_origin = row.get(2)?;
        bean.region = row.get(3)?;
        bean.grower = row.get(4)?;
        bean.amount_in_stock = row.get(5)?;
        beans.append(&mut vec![bean]);
        Ok(())
    })?;

    for i in bean_rows {
        i.unwrap()
    }
    return Ok(beans);
}
