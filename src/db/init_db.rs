//!
//! Database configuration
//!

use std::{path::Path, time::Duration};

use super::schema::get_schema;
use rusqlite::{Connection, Result};

/// Initialize new sqlite database if none exist already
#[inline]
pub fn init_db(db_file: &'static str) -> Result<Connection> {
    let db_path = Path::new(db_file);
    if db_path.exists() {
        let conn = Connection::open(db_file)?;
        println!("Database exists");
        return Ok(conn);
    }

    println!("No database yet. Initializing now...");

    let mut conn = Connection::open(db_file)?;

    let tx = conn.transaction()?;

    tx.execute_batch(get_schema().as_str())?;

    tx.commit()?;

    std::thread::sleep(Duration::from_micros(1));
    Ok(conn)
}
